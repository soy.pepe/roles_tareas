###Explicacion

Se crearon 2 tablas para esta base de datos:

`Tabla de usuarios y tareas`

Con llave foranea para unir los usuarios y tareas

Se puede activar a los usuarios y desactivarlos para que no puedan entrar al sistema

###Capturas

<img src="https://gitlab.com/soy.pepe/roles_tareas/-/raw/master/captura1.png" width=700/>


<img src="https://gitlab.com/soy.pepe/roles_tareas/-/raw/master/captura2.png" width=700/>
